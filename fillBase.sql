INSERT INTO `kartina`.`user`
(`civ`,
`date_inscr`,
`email`,
`nom`,
`prenom`,
`profil`,
`pwd`,
`tel`,
`version`)
VALUES
("mr",
"2019-04-16",
"admin@kartinabmsy.com",
"Meen",
"Ed",
"admin",
"pwdAdmin",
"0101010101",
0);
INSERT INTO `kartina`.`user`
(`civ`,
`date_inscr`,
`email`,
`nom`,
`prenom`,
`profil`,
`pwd`,
`tel`,
`version`)
VALUES
("mlle",
"2018-12-11",
"drira.yasmine@caramail.com",
"Drira",
"Yasmine",
"photographe",
"dyaz",
"0606060606",
0);
INSERT INTO `kartina`.`user`
(`civ`,
`date_inscr`,
`email`,
`nom`,
`prenom`,
`profil`,
`pwd`,
`tel`,
`version`)
VALUES
("mme",
"2020-01-15",
"lauredinateur@yahoo.fr",
"Dinateur",
"Laure",
"client",
"ordi",
"0123456789",
0);
INSERT INTO `kartina`.`user`
(`civ`,
`date_inscr`,
`email`,
`nom`,
`prenom`,
`profil`,
`pwd`,
`tel`,
`version`)
VALUES
("mr",
"2019-02-10",
"artograf@yahoo.fr",
"Stephoto",
"Arty",
"photographe",
"arto",
"0234567891",
0);
INSERT INTO `kartina`.`theme`
(`descr`,
`theme`,
`version`)
VALUES
("Old fashion",
"Noir et blanc",
0);
INSERT INTO `kartina`.`theme`
(`descr`,
`theme`,
`version`)
VALUES
("Là-bas",
"Voyage",
0);
INSERT INTO `kartina`.`theme`
(`descr`,
`theme`,
`version`)
VALUES
("Toute la beauté au naturel",
"Nature",
0);
INSERT INTO `kartina`.`theme`
(`descr`,
`theme`,
`version`)
VALUES
("Insolite ou beau, l'art urbain sous toutes ses formes",
"Urban",
0);
INSERT INTO `kartina`.`theme`
(`descr`,
`theme`,
`version`)
VALUES
("Tout en douceur",
"Femme",
0);
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-01-01",
"Bonne année",
"100",
473,
774,
"paysage",
130,
100,
500,
421,
"2020",
0,
2,
"https://image.shutterstock.com/image-photo/silhouette-engineer-construction-team-work-600w-1559809274.jpg");
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-03-11",
"Illuminer nos fêtes",
"001",
640,
960,
"paysage",
260,
200,
800,
610,
"Festif",
0,
2,
"https://cdn.pixabay.com/photo/2014/11/21/03/29/fair-540126_960_720.jpg");
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-01-01",
"Déco old fashion",
"100",
675,
540,
"portrait",
195,
150,
1000,
91,
"Nostalgie",
0,
2,
"https://cdn.pixabay.com/photo/2018/07/28/01/08/vintage-3567130_960_720.jpg");
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-01-01",
"Vue d'artiste",
"100",
540,
960,
"paysage",
520,
400,
500,
421,
"Dreamy World",
0,
2,
"https://cdn.pixabay.com/photo/2016/04/14/13/06/landscape-1328858_960_720.jpg");
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(4,
2);
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-02-07",
"La dame au tigre blanc",
"111",
640,
960,
"paysage",
156,
120,
800,
316,
"Beauté fatale",
0,
2,
"https://cdn.pixabay.com/photo/2018/01/25/14/12/nature-3106213_960_720.jpg");
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(5,
3);
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(5,
5);
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2019-11-16",
"Restons légers",
"011",
623,
960,
"paysage",
104,
80,
1000,
107,
"Eléphant",
0,
2,
"https://cdn.pixabay.com/photo/2017/10/20/10/58/elephant-2870777_960_720.jpg");
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(6,
1);
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(6,
3);
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-01-01",
"Les rues de mon enfance",
"100",
720,
960,
"paysage",
520,
400,
500,
421,
"Pavés",
0,
4,
"https://cdn.pixabay.com/photo/2015/09/21/13/17/road-949832_960_720.jpg");
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(7,
1);
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(7,
4);
INSERT INTO `kartina`.`photographie`
(`date_ajout`,
`descr`,
`dispo_format`,
`hauteur`,
`largeur`,
`orientation`,
`prix_affiche`,
`prix_initial`,
`stock_initial`,
`stock_restant`,
`titre`,
`version`,
`photographe`,
`url`)
VALUES
("2020-02-02",
"Vie courante au Burkina Faso",
"100",
720,
960,
"paysage",
260,
200,
800,
639,
"Puits",
0,
4,
"https://cdn.pixabay.com/photo/2013/08/06/15/41/africa-170205_960_720.jpg");
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(8,
2);
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(8,
4);
INSERT INTO `kartina`.`photo_theme`
(`photo`,
`theme`)
VALUES
(8,
5);
INSERT INTO `kartina`.`format`
(`version`,
`descr`,
`hauteur`,
`largeur`,
`libelle`,
`prct`)
VALUES
(0,
"Tirage encadré, édition limitée à 5000 exemplaires dans le monde",
24,
30,
"classique",
100);
INSERT INTO `kartina`.`format`
(`version`,
`descr`,
`hauteur`,
`largeur`,
`libelle`,
`prct`)
VALUES
(0,
"Photographie montée sur aluminium, édition limitée à 500 exemplaires",
60,
75,
"grand",
200);
INSERT INTO `kartina`.`format`
(`version`,
`descr`,
`hauteur`,
`largeur`,
`libelle`,
`prct`)
VALUES
(0,
"Photographie montée sur aluminium, édition limitée à 200 exemplaires",
100,
125,
"géant",
400);
INSERT INTO `kartina`.`format`
(`version`,
`descr`,
`hauteur`,
`largeur`,
`libelle`,
`prct`)
VALUES
(0,
"Photographie montée sur aluminium, édition limitée à 100 exemplaires",
125,
150,
"collector",
1000);
INSERT INTO `kartina`.`finition`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Passe-partout noir avec liseré blanc",
"Blackout",
100,
0);
INSERT INTO `kartina`.`finition`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Passe-partout blanc",
"Artshot",
140,
0);
INSERT INTO `kartina`.`finition`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Tirage contrecollé sur support aluminium",
"Support aluminium",
260,
0);
INSERT INTO `kartina`.`finition`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Tirage contrecollé sur support aluminium avec finition protectrice en verre acrylique accentuant les contrastes et les couleurs/Finition de référence.",
"Support aluminium avec verre acrylique",
335,
0);
INSERT INTO `kartina`.`finition`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Tirage sur papier photo expédié roulé, à accrocher ou à encadrer",
"Tirage sur papier photo",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Format 50x40cm",
"Aluminium noir",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Format 50x40cm",
"Bois blanc",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Format 50x40cm",
"Acajou mat",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Format 50x40cm",
"Aluminium brossé",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Tirage contrecollé sur support aluminium",
"Sans encadrement",
100,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Caisse américaine noire satinée assemblée à la main, apportant un effet de luminosité et de profondeur/Le choix des galeristes",
"Encadrement noir satin",
145,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Caisse américaine blanche satinée assemblée à la main, apportant un effet de luminosité et de profondeur",
"Encadrement blanc satin",
145,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Caisse américaine en noyer massif assemblée à la main, apportant un effet de luminosité et de profondeur",
"Encadrement noyer",
145,
0);
INSERT INTO `kartina`.`cadre`
(`descr`,
`libelle`,
`prct`,
`version`)
VALUES
("Caisse américaine en chêne massif assemblée à la main, apportant un effet de luminosité et de profondeur",
"Encadrement chêne",
145,
0);
INSERT INTO `kartina`.`commande`
(`date_commande`,
`etat`,
`prix_total`,
`version`,
`user`)
VALUES
("2020-02-01",
"livree",
0,
0,
3);
INSERT INTO `kartina`.`commande`
(`date_commande`,
`etat`,
`prix_total`,
`version`,
`user`)
VALUES
("2020-03-15",
"attente",
0,
0,
3);
INSERT INTO `kartina`.`article`
(`prix_unit`,
`qte`,
`version`,
`cadre`,
`commande`,
`finition`,
`format`,
`photo`)
VALUES
(0,
1,
0,
2,
1,
2,
1,
1);
INSERT INTO `kartina`.`article`
(`prix_unit`,
`qte`,
`version`,
`cadre`,
`commande`,
`finition`,
`format`,
`photo`)
VALUES
(0,
1,
0,
6,
1,
3,
2,
3);
INSERT INTO `kartina`.`article`
(`prix_unit`,
`qte`,
`version`,
`cadre`,
`commande`,
`finition`,
`format`,
`photo`)
VALUES
(0,
2,
0,
9,
1,
4,
3,
4);
INSERT INTO `kartina`.`article`
(`prix_unit`,
`qte`,
`version`,
`cadre`,
`commande`,
`finition`,
`format`,
`photo`)
VALUES
(0,
4,
0,
null,
2,
5,
2,
1);
INSERT INTO `kartina`.`info_photographe`
(`bio`,
`facebook`,
`pinterest`,
`twitter`,
`version`,
`user`)
VALUES
("Artiste incarnant la douceur",
"",
"",
"",
0,
2);
INSERT INTO `kartina`.`tag`
(`tag`,
`version`)
VALUES
("futuriste",
0);
INSERT INTO `kartina`.`photo_tag`
(`photo`,
`tag`)
VALUES
(4,
1);
INSERT INTO `kartina`.`adresse`
(`cp`,
`is_active`,
`num`,
`pays`,
`rue`,
`version`,
`ville`,
`user`)
VALUES
("75013",
TRUE,
134,
"France",
"Rue de Tolbiac",
0,
"Paris",
2);
INSERT INTO `kartina`.`adresse`
(`cp`,
`is_active`,
`num`,
`pays`,
`rue`,
`version`,
`ville`,
`user`)
VALUES
("84130",
TRUE,
7,
"France",
"Rue des Camélias",
0,
"Le Pontet",
3);