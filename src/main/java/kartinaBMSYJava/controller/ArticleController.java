package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Article;
import kartinaBMSYJava.repository.ArticleRepository;

@RestController
@RequestMapping("/article")
public class ArticleController {
	@Autowired
	private ArticleRepository articleRepository;
	
	@GetMapping("article")
	public List<Article> all(){
		return articleRepository.findAll();
	}
	
	@PostMapping("article")
	public void create(@RequestBody Article a) {
		articleRepository.save(a);
	}

}
