package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Cadre;
import kartinaBMSYJava.repository.CadreRepository;

@RestController
@RequestMapping("/cadre")
public class CadreController {
	@Autowired
	private CadreRepository cadreRepository;
	
	@GetMapping("cadre")
	public List<Cadre> all(){
		return cadreRepository.findAll();
	}

}
