package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Commande;
import kartinaBMSYJava.repository.CommandeRepository;


@RestController
@RequestMapping("/commande")
public class CommandeController {
	@Autowired
	private CommandeRepository commandeRepository;
	@GetMapping("commande")
	public List<Commande> all(){
		return commandeRepository.findAll();
	}
	
	@PostMapping("commande")
	public void create(@RequestBody Commande c) {
		commandeRepository.save(c);
	}

}
