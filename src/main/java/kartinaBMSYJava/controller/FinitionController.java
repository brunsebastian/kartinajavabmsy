package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Finition;
import kartinaBMSYJava.repository.FinitionRepository;

@RestController
@RequestMapping("/finition")
public class FinitionController {
	@Autowired
	private FinitionRepository finitionRepository;
	
	@GetMapping("finition")
	public List<Finition> all(){
		return finitionRepository.findAll();
	}

}
