package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Format;
import kartinaBMSYJava.repository.FormatRepository;

@RestController
@RequestMapping("/format")
@CrossOrigin("*")
public class FormatController {
	@Autowired
	private FormatRepository formatRepository;
	
	@GetMapping("format")
	public List<Format> all(){
		return formatRepository.findAll();
	}

}
