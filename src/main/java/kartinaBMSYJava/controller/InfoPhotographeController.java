package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.InfoPhotographe;
import kartinaBMSYJava.repository.InfoPhotographeRepository;

@RestController
@RequestMapping("infoPhotographe")
public class InfoPhotographeController {
	
	@Autowired
	private InfoPhotographeRepository infoPhotographeRepository;
	
	@GetMapping("info")
	public List<InfoPhotographe> all(){
		return infoPhotographeRepository.findAll();
	}
	

}
