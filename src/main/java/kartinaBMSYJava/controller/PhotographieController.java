package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Photographie;
import kartinaBMSYJava.repository.PhotographieRepository;


@RestController
@RequestMapping("/photo")
public class PhotographieController {
	
	@Autowired
	private PhotographieRepository photoRepository;
	
	@GetMapping("photo")
	public List<Photographie> all(){
		return photoRepository.findAll();
	}
	
	@GetMapping("photo/{id}")
	public Photographie findById (@PathVariable(name = "id") Long id){
		return photoRepository.findById(id).get();
	}
	
	@PostMapping("photo")
	public void create(@RequestBody Photographie p) {
		photoRepository.save(p);
	}
	
	@PutMapping("photo")
	public void update(@RequestBody Photographie p) {
		//Photographie photo = photoRepository.findById(p.getId()).get(); 
		//photo.setTitre(p.getTitre());
		//photoRepository.save(photo);
	}
	
	@DeleteMapping("photo")
	public void delete(@RequestBody Photographie p) {
		photoRepository.delete(p);
	}
	
	//a test
	@GetMapping("nouveaute")
	public List<Photographie> nouveaute(){
		return photoRepository.nouveaute();
	}
	
	//a test
	@GetMapping("voyage")
	public List<Photographie> themeVoyage(){
		return photoRepository.themeVoyage();
	}
	
	//a test
	@GetMapping("noiretblanc")
	public List<Photographie> themeNoirEtBlanc(){
		return photoRepository.themeNoirEtBlanc();
	}
	
	
	@GetMapping("derniers")
	public List<Photographie> derniersExemplaires(){
		return photoRepository.derniersExemplaires();
	}
	
	@GetMapping("top6Ventes")
	public List<Photographie> top6Ventes(){
		return photoRepository.top6Ventes();
	}
	
	@GetMapping("photos/paginate/{page}/{size}")
	public Page<Photographie> getPhotoPagine(@PathVariable("page") int page, @PathVariable("size")int size) {
		Pageable pageRequest = PageRequest.of(page, size);
		return photoRepository.findAll(pageRequest);
	}
	@GetMapping("portfolio/{id}")
	public List<Photographie> findPortfolio(@PathVariable(name = "id") long id){
		return photoRepository.findPhotographe(id);
	}
}
