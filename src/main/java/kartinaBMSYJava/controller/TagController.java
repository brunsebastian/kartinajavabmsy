package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Tag;
import kartinaBMSYJava.repository.TagRepository;

@RestController
@RequestMapping("/tag")
public class TagController {
	@Autowired
	private TagRepository tagRepository;

	@GetMapping("tag")
	public List<Tag> all(){
		return tagRepository.findAll();
	}
}
