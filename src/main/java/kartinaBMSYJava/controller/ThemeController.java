package kartinaBMSYJava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kartinaBMSYJava.model.Theme;
import kartinaBMSYJava.model.User;
import kartinaBMSYJava.repository.ThemeRepository;

@RestController
@RequestMapping("/theme")
public class ThemeController {
	@Autowired
	private ThemeRepository themeRepository;
	
	@GetMapping("theme")
	public List<Theme> all(){
		return themeRepository.findAll();
	}
	
	@PostMapping("theme")
	public void create(@RequestBody Theme t) {
		themeRepository.save(t);
	}

}