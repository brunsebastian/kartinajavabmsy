package kartinaBMSYJava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import kartinaBMSYJava.model.Photographie;
import kartinaBMSYJava.model.User;
import kartinaBMSYJava.repository.PhotographieRepository;
import kartinaBMSYJava.repository.UserRepository;

import java.util.*;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("user")
	public List<User> all(){
		return userRepository.findAll();
	}
	
	@GetMapping("user/{id}")
	public User findById (@PathVariable(name = "id") Long id){
		return userRepository.findById(id).get();
	}
	
	@PostMapping("user")
	public void create(@RequestBody User u) {
		userRepository.save(u);
	}
	
	@PutMapping("user")
	public void updateProfil(@RequestBody User u) {
		User user = userRepository.findById(u.getId()).get(); 
		user.setProfil(u.getProfil());
		userRepository.save(user);
	}
	
	@DeleteMapping("user")
	public void delete(@RequestBody User u) {
		userRepository.delete(u);
	}
	
	@GetMapping("photographe")
	public List<User> allPhotographe(){
		return userRepository.userPhotographe();
	}
	@GetMapping("photographe/{id}")
	public User photographeById(@PathVariable(name = "id") Long id){
		return userRepository.findById(id).get();
	}
}
