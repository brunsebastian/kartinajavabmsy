package kartinaBMSYJava.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class Article {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="commande")
	private Commande cmd;
	@ManyToOne
	@JoinColumn(name="photo")
	private Photographie photo;
	private int qte,prixUnit;
	@ManyToOne
	@JoinColumn(name="format")
	private Format format;
	@ManyToOne
	@JoinColumn(name="finition")
	private Finition finition;
	@ManyToOne
	@JoinColumn(name="cadre",nullable=true)
	private Cadre cadre;
	@Version
	private int version;
	public Article() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Commande getCmd() {
		return cmd;
	}
	public void setCmd(Commande cmd) {
		this.cmd = cmd;
	}
	public Photographie getPhoto() {
		return photo;
	}
	public void setPhoto(Photographie photo) {
		this.photo = photo;
	}
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	public int getPrixUnit() {
		return prixUnit;
	}
	public void setPrixUnit(int prixUnit) {
		this.prixUnit = prixUnit;
	}
	public Format getFormat() {
		return format;
	}
	public void setFormat(Format format) {
		this.format = format;
	}
	public Finition getFinition() {
		return finition;
	}
	public void setFinition(Finition finition) {
		this.finition = finition;
	}
	public Cadre getCadre() {
		return cadre;
	}
	public void setCadre(Cadre cadre) {
		this.cadre = cadre;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}
