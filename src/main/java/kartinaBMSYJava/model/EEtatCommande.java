package kartinaBMSYJava.model;

public enum EEtatCommande {
	attente,
	preparation,
	acheminement,
	livree;
}
