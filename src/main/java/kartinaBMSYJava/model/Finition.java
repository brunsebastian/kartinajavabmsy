package kartinaBMSYJava.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Finition {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String libelle,descr;
	private int prct;
	@JsonIgnore
	@OneToMany(mappedBy="finition",fetch = FetchType.LAZY)
	private Collection<Article> articles=new ArrayList<Article>();
	@Version
	private int version;
	public Finition() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public int getPrct() {
		return prct;
	}
	public void setPrct(int prct) {
		this.prct = prct;
	}
	public Collection<Article> getArticles() {
		return articles;
	}
	public void setArticles(Collection<Article> articles) {
		this.articles = articles;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}
