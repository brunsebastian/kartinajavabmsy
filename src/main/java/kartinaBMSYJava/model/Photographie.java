package kartinaBMSYJava.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Photographie {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String titre,descr,dispoFormat,url;
	@ManyToOne
	@JoinColumn(name="photographe")
	private User photographe;
	private Date dateAjout;
	private int prixInitial,prixAffiche,hauteur,largeur,stockInitial,stockRestant;
	@Enumerated(EnumType.STRING)
	private EOrientation orientation;
	@JsonIgnore
	@OneToMany(mappedBy="photo",fetch = FetchType.LAZY)
	private Collection<Article> arts=new ArrayList<Article>();
	@ManyToMany
	@JoinTable(name="PhotoTag",
	joinColumns=@JoinColumn(name="photo"),
	inverseJoinColumns=@JoinColumn(name="tag"))
	private Collection<Tag> tags=new ArrayList<Tag>();
	@ManyToMany
	@JoinTable(name="PhotoTheme",
	joinColumns=@JoinColumn(name="photo"),
	inverseJoinColumns=@JoinColumn(name="theme"))
	private Collection<Theme> themes=new ArrayList<Theme>();
	@Version
	private int version;
	public Photographie() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getDispoFormat() {
		return dispoFormat;
	}
	public void setDispoFormat(String dispoFormat) {
		this.dispoFormat = dispoFormat;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public User getPhotographe() {
		return photographe;
	}
	public void setPhotographe(User photographe) {
		this.photographe = photographe;
	}
	public Date getDateAjout() {
		return dateAjout;
	}
	public void setDateAjout(Date dateAjout) {
		this.dateAjout = dateAjout;
	}
	public int getPrixInitial() {
		return prixInitial;
	}
	public void setPrixInitial(int prixInitial) {
		this.prixInitial = prixInitial;
	}
	public int getPrixAffiche() {
		return prixAffiche;
	}
	public void setPrixAffiche(int prixAffiche) {
		this.prixAffiche = prixAffiche;
	}
	public int getHauteur() {
		return hauteur;
	}
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	public int getStockInitial() {
		return stockInitial;
	}
	public void setStockInitial(int stockInitial) {
		this.stockInitial = stockInitial;
	}
	public int getStockRestant() {
		return stockRestant;
	}
	public void setStockRestant(int stockRestant) {
		this.stockRestant = stockRestant;
	}
	public EOrientation getOrientation() {
		return orientation;
	}
	public void setOrientation(EOrientation orientation) {
		this.orientation = orientation;
	}
	public Collection<Article> getArts() {
		return arts;
	}
	public void setArts(Collection<Article> arts) {
		this.arts = arts;
	}
	public Collection<Tag> getTags() {
		return tags;
	}
	public void setTags(Collection<Tag> tags) {
		this.tags = tags;
	}
	public Collection<Theme> getThemes() {
		return themes;
	}
	public void setThemes(Collection<Theme> themes) {
		this.themes = themes;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}