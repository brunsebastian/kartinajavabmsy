package kartinaBMSYJava.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Theme {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String theme,descr;
	@JsonIgnore
	@ManyToMany(mappedBy="themes")
	private Collection<Photographie> photos=new ArrayList<Photographie>();
	@Version
	private int version;
	public Theme() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public Collection<Photographie> getPhotos() {
		return photos;
	}
	public void setPhotos(Collection<Photographie> photos) {
		this.photos = photos;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}
