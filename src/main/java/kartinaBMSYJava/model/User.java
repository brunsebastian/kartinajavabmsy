package kartinaBMSYJava.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@Enumerated(EnumType.STRING)
	private ECivilite civ;
	private String nom,prenom,email,pwd,tel;
	@Enumerated(EnumType.STRING)
	private EProfil profil;
	@OneToMany(mappedBy="user")
	private Collection<Adresse> adr=new ArrayList<Adresse>();
    @OneToOne(mappedBy = "user",fetch = FetchType.LAZY, optional = true)
	private InfoPhotographe info;
	private Date dateInscr;
	@JsonIgnore
	@OneToMany(mappedBy="user")
	private Collection<Commande> cmds=new ArrayList<Commande>();
	@JsonIgnore
	@OneToMany(mappedBy="photographe")
	private Collection<Photographie> portfolio=new ArrayList<Photographie>();
	@Version
	private int version;
	public User() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ECivilite getCiv() {
		return civ;
	}
	public void setCiv(ECivilite civ) {
		this.civ = civ;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public EProfil getProfil() {
		return profil;
	}
	public void setProfil(EProfil profil) {
		this.profil = profil;
	}
	public Collection<Adresse> getAdr() {
		return adr;
	}
	public void setAdr(Collection<Adresse> adr) {
		this.adr = adr;
	}
	public InfoPhotographe getInfo() {
		return info;
	}
	public void setInfo(InfoPhotographe info) {
		this.info = info;
	}
	public Date getDateInscr() {
		return dateInscr;
	}
	public void setDateInscr(Date dateInscr) {
		this.dateInscr = dateInscr;
	}
	public Collection<Commande> getCmds() {
		return cmds;
	}
	public void setCmds(Collection<Commande> cmd) {
		this.cmds = cmd;
	}
	public Collection<Photographie> getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(Collection<Photographie> portfolio) {
		this.portfolio = portfolio;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}

