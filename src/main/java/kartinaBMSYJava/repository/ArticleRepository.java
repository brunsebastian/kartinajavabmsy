package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Long>{

}
