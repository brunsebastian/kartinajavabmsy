package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Cadre;

public interface CadreRepository extends JpaRepository<Cadre, Long>{

}
