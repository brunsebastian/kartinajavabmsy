package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Long>{

}
