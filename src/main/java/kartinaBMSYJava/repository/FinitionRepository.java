package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Finition;

public interface FinitionRepository extends JpaRepository<Finition, Long>{

}
