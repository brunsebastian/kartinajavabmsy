package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Format;

public interface FormatRepository extends JpaRepository<Format, Long>{

}
