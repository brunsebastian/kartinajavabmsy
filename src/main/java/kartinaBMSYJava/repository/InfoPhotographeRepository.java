package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.InfoPhotographe;

public interface InfoPhotographeRepository extends JpaRepository<InfoPhotographe, Long>{

}
