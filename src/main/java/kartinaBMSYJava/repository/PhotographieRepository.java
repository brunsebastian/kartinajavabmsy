package kartinaBMSYJava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import kartinaBMSYJava.model.Photographie;


public interface PhotographieRepository extends JpaRepository<Photographie, Long>{
	@Query(value="select p.* from Photographie p order by p.date_ajout desc limit 24", nativeQuery=true )
	List<Photographie> nouveaute ();
	
	@Query(value="select p.* from Photographie p where p.theme = 'Voyage'", nativeQuery=true )
	List<Photographie> themeVoyage ();
	
	@Query(value="select p.* from Photographie p where p.theme = 'Noir et Blanc'", nativeQuery=true )
	List<Photographie> themeNoirEtBlanc ();
	

	@Query(value="select p.* from Photographie p where p.stock_initial/p.stock_restant>10", nativeQuery=true )
	List<Photographie> derniersExemplaires();
	
	@Query(value="select p.* from photographie p join article a on p.id=a.photo group by a.photo order by sum(a.qte) desc limit 6", nativeQuery=true )
	List<Photographie> top6Ventes();
	
	@Query(value="select p.* from photographie p where p.photographe=?1", nativeQuery=true )
	List<Photographie> findPhotographe(Long photographe);
}
