package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Long>{

}
