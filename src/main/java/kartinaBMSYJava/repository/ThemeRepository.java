package kartinaBMSYJava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kartinaBMSYJava.model.Theme;

public interface ThemeRepository extends JpaRepository<Theme, Long>{

}
