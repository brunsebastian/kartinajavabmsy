package kartinaBMSYJava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import kartinaBMSYJava.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	@Query("select u from User u where u.profil = 'photographe'")
	List<User> userPhotographe ();

}
